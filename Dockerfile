FROM docker.io/golang:alpine3.16

RUN addgroup -S nonroot && adduser -S nonroot -G nonroot

USER nonroot

RUN mkdir /home/nonroot/src
COPY /app/main.go /home/nonroot/src
RUN cd /home/nonroot/src && go env -w GO111MODULE=off &&\
    go build -o goapp

RUN mkdir /home/nonroot/app &&\
    cp /home/nonroot/src/goapp /home/nonroot/app && rm -rf /home/nonroot/src

EXPOSE 8080

ENTRYPOINT [ "/home/nonroot/app/goapp" ]
