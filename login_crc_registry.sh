#!/bin/bash

# podman login default-route-openshift-image-registry.apps-crc.testing --tls-verify=false

podman login -u `oc whoami` -p `oc whoami --show-token` `oc registry info` --tls-verify=false
