SHELL := /bin/bash
NAME = test-container
REGISTRY=docker.io/embrance
VERSION=0.5

all: clean build

default: build

build:
	podman build -t $(NAME):$(VERSION) . --no-cache

scratchbuild:
	podman build -f Dockerfile.scratch -t $(NAME):$(VERSION) .

multistagebuild:
	podman build -f Dockerfile.multistage -t $(NAME):$(VERSION) .

push: scratchbuild
	podman push $(NAME):$(VERSION)

run:
	podman run -p 8081:8080 --name=$(NAME) -d $(NAME):$(VERSION)

runti:
	podman run --rm -p 8081:8080 -ti $(NAME):$(VERSION)

clean:
	-podman rm -f $(NAME)
	-podman rmi $(NAME):$(VERSION)

publish: build
	-podman tag localhost/$(NAME):$(VERSION) $(REGISTRY)/$(NAME):$(VERSION)
	-podman push $(REGISTRY)/$(NAME):$(VERSION)
